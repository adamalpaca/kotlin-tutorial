# Kotlin Test

This project essentially regroups some examples from [this tutorial](https://play.kotlinlang.org/byExample/overview).

The idea is to understand the basic syntax and concepts of the Kotlin programming language.

The different Kotlin files are found in this repository: [Kotlin Test](https://bitbucket.org/adamalpaca/kotlin-tutorial/src/master/src/org/kotlintest/)