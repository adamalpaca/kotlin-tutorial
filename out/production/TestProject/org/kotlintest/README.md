# Kotlin Files With Examples

## [First Try](https://bitbucket.org/adamalpaca/kotlin-tutorial/src/master/src/org/kotlintest/firsttry/)

Use of an extension function on a String array to sort using an insertion sort. Just a small test of a Kotlin mechanism.

## [Introduction](https://bitbucket.org/adamalpaca/kotlin-tutorial/src/master/src/org/kotlintest/introduction/)

 1. **Functions** : infix functions, variable argument quantities
 2. **Generics** : use of a template type to create collections and the like
 3. **Inheritance** : similar to other languages

## [Control Flow](https://bitbucket.org/adamalpaca/kotlin-tutorial/src/master/src/org/kotlintest/controlflow/)

 1. **When** statements / expressions : used as a replacement for Switch cases in other programming languages
 2. **Loops** : includes iterators on custom classes
 3. **Ranges** : ranges in for loops / if statements

## [Special Classes](https://bitbucket.org/adamalpaca/kotlin-tutorial/src/master/src/org/kotlintest/specialclasses/)

 1. **Data classes** : used for manipulating data sets
 2. **Enum classes** : similar to other programming languages, can also contain properties
 3. **[Sealed](https://www.google.com/search?q=seal&client=firefox-b-d&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjP0PeeqNrjAhU-xcQBHR6bD4cQ_AUIESgB&biw=1680&bih=924) classes** ![alt text](misc/seal.png "Sealed .. get it?"): classes that can only be inherited in the same file, to restrict child classes 
 4. **Object keyword** : used for defining singular objects, similar to singletons

## [Functional](https://bitbucket.org/adamalpaca/kotlin-tutorial/src/master/src/org/kotlintest/functional/)

 1. **Higher Order Functions** : passing functions as parameters or return functions as parameters
 2. **Lambda Functions** : quick throwaway functions or on the fly definitons
 3. **Extension Functions / Properties** : allows you to *statically* extend the methods in a class or their properties

## [Collections](https://bitbucket.org/adamalpaca/kotlin-tutorial/src/master/src/org/kotlintest/collections/)

 1. **Lists, Sets, Maps** : different types of collections (ordered, unordered/unique and key-value pairs respectively)
 2. **Filtering, mapping** : regrouping/filtering of collections using predicates as lambda parameters
 3. **Searching** : different methodfs for finding elements in a collection or splitting this collection using predicates
 4. **Flat Mapping, Sorting, Element Access** : how to flatten a collection according to a certain instruction, sorting, zipping together collections and access of elements with default in case of null