package org.kotlintest.controlflow


fun main (args : Array<String>)
{
    cases("Hello")
    cases(1)
    cases(0L)
    cases(MyClass())
    cases("hello")

    println(whenAssign("Hello"))
    println(whenAssign(1))
    println(whenAssign(0L))
    println(whenAssign(MyClass()))
    println(whenAssign("hello"))
}

fun cases(obj: Any) {
    when (obj) {
        1 -> println("One")
        "Hello" -> println("Greeting")
        is Long -> println("Long")
        is MyClass -> println("Instance of MyClass")
        !is String -> println("Not a string")
        else -> println("Unknown")
    }
}

fun whenAssign(obj: Any): Any {
    val result = when(obj) {
        1 -> "One"
        "Hello" -> 1
        is Long -> false
        is MyClass -> true
        !is String -> "Not a string"
        else -> "Unknown"
    }
    return result
}

class MyClass