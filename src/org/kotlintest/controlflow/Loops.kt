package org.kotlintest.controlflow

class Song(val name: String)

class Album(val name: String, val artist: String, val releaseYear: Int, val songs: List<Song>) {

    operator fun iterator() : Iterator<Song> {
        return songs.iterator()
    }
}

fun main( args: Array<String>) {

    val album = Album("Battle Born", "The Killers", 2013, listOf(Song("Flesh and Bone"), Song("Runaways"), Song("Miss Atomic Bomb")))

    for (song in album)
        println(song.name)
}