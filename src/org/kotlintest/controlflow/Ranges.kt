package org.kotlintest.controlflow

fun main (args : Array<String>) {

    // For loop with range defined for int
    for (i in 0..3)
        print("$i, ")
    println("")

    // For loop with steps defined
    for(i in 0..10 step 2)
        print("$i, ")
    println("")

    // Inverted direction
    for(i in 'z' downTo 'c' step 2)
        print("$i, ")
    println("")

    // Is something contained within a range
    val y = 33
    if(y !in 30..40 step 2) println("*y isn't in even numbers between 30 and 40*") else println("*sad noises*")

}