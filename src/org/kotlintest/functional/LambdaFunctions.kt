package org.kotlintest.functional

val split1: (Double) -> Double = {a: Double -> a/2.0}
val split2: (Double) -> Double = {a -> a/2.0}
val split3 = {a: Double -> a/2.0}
val split4: (Double) -> Double = {it/2.0}
val uppercase1: (String) -> String = String::toUpperCase

fun main (args: Array<String>) {
    println(split1(150.0))
    println(split2(150.0))
    println(split3(150.0))
    println(split4(150.0))
    println(uppercase1("Hello there. General Kenobi"))
}