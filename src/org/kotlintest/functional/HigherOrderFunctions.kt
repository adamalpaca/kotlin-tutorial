package org.kotlintest.functional

// Taking a function as parameter
fun calculate(x: Int, y: Int, operation: (Int, Int) -> Int): Int {
    return operation(x, y)
}

fun sum(x: Int, y: Int): Int = x + y


// Returning functions
fun operation(): (Int) -> Int {
    return ::square
}

fun square(x: Int): Int = x * x


fun main (args: Array<String>) {
    // Passing a function as a parameter
    val sumResult = calculate(4, 5, ::sum)              // Use "::" to reference the function
    val multResult = calculate(4, 5) { a, b -> a * b }

    println("Sum result: $sumResult, Mult result: $multResult")

    // Taking function as return parameter
    val func = operation()
    println("Result of operation = ${func(2)}")
}