package org.kotlintest.functional

// Define some classes
data class Person(val name: String, val age: Int)
data class Group(val people: Collection<Person>)

// Create some extension functions
fun Group.oldestPersonName(): String = this.people.maxBy { it.age }?.name?: ""
fun Group.oldestPersonAge(): Int = this.people.maxBy { it.age }?.age?: 0
fun Group.printOldestPerson() {println("The oldest person is ${oldestPersonName()} at ${oldestPersonAge()} years old")}

val Group.commaDelimitedNames: String
    get() = people.map { it.name + " : " + it.age}.joinToString()

// Extension function on any type
fun <T> T?.nullSafeToString() = this?.toString() ?: "Null safe string"

var test: Boolean = true

fun main (args: Array<String>) {

    // Testing out the extension functions
    val assembly = Group(listOf(Person("John", 54), Person("Glen", 121), Person("Betina", 4), Person("Bloopy", 1)))
    assembly.printOldestPerson()
    println(assembly.commaDelimitedNames)

    // Nullable group created to test our null safe extension function
    var anotherGroup: Group? = Group(listOf(Person("Mr. Sir", 83)))
    if(test)
        anotherGroup = null
    println(anotherGroup.nullSafeToString())
}