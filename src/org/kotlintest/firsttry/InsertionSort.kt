package org.kotlintest.firsttry

import java.io.File
import java.util.*

fun Vector<String>.arrangeInAlphabeticalOrder()
{
    val newList: Vector<String> = Vector()

    // Add elements to new list
    newList.insertElementAt(this[0], 0)

    // Insertion sort
    var inserted = false

    for(w in 1 until this.size) {
        if (this[w].toLowerCase() < newList[0].toLowerCase()) {
            newList.insertElementAt(this[w], 0)
            inserted = true
        } else for(i in 0 until newList.size-1)
        {
            if(this[w].toLowerCase() >= newList[i].toLowerCase() && this[w].toLowerCase() <= newList[i+1].toLowerCase()) {
                newList.insertElementAt(this[w], i+1)
                inserted = true
                break
            }
        }
        if(!inserted)
            newList.insertElementAt(this[w], newList.size)

        inserted = false
    }

    // Purge list
    removeAllElements()

    // Insert the elements from the new list
    for(w in newList)
        addElement(w)
}

fun main(args: Array<String>) {

    val listOfWords: Vector<String> = Vector()

    File("data.txt").forEachLine {
        val words = it.split(" ", ".", System.lineSeparator())
        for(w in words)
            listOfWords.addElement(w)
    }

    listOfWords.arrangeInAlphabeticalOrder()

    for(e in listOfWords)
        println(e)
}