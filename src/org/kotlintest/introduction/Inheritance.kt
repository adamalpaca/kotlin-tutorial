package org.kotlintest.introduction

open class Brass (val noise: String){
    open fun sound() {
        println(noise)
    }
}
class Trumpet : Brass("Doot doot")
class Tuba : Brass("Prrffffff")

fun main (args: Array<String>) {
    val trump : Brass = Trumpet()
    val tuba : Brass = Tuba()
    val genericBrass: Brass = Brass("Toot toot")

    trump.sound()
    tuba.sound()
    genericBrass.sound()
}