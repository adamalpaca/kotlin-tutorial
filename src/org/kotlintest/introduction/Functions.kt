package org.kotlintest.introduction

fun main(args: Array<String>) {

    // Create an infix function for integers that repeats a string
    infix fun Int.times(str: String) = str.repeat(this)
    println(4 times "Bye ")

    // Using food functions
    val chorizo = Food("Chorizo")
    val olive = Food("Olive")
    val tomato = Food("Tomato")
    val jalepeño = Food("Jalepeño")
    val bread = Food("Bread")
    val onion = Food("Onion")
    val cheese = Food("Cheese")
    val cream = Food("Sour Cream")
    tomato complements chorizo
    olive complements chorizo
    jalepeño complements chorizo
    bread complements chorizo
    onion complements chorizo
    cheese complements chorizo
    cream complements chorizo
    chorizo.goesWith()

    // Testing operator overload
    for (c in chorizo[0..3])
        println("${c.name}")

    // Testing print all function
    printAll("Hello", "Are you well?", "I've been considering things")
}

class Food(val name : String) {
    val complementaryFoods = mutableListOf<Food>()
    infix fun complements(other: Food){ other.complementaryFoods.add(this) }

    fun goesWith(){
        println("$name goes with :")
        for(c in complementaryFoods)
            println("${c.name}")
    }

    operator fun get(range: IntRange): MutableList<Food> {
        val newList = mutableListOf<Food>()
        for(i in range) {
            if(i>0) newList.add(newList.size - 1, complementaryFoods[i]) else newList.add(complementaryFoods[i])
        }
        return newList
    }
}


fun printAll(vararg messages: String) {
    for(m in messages) println(m)
}