package org.kotlintest.introduction

class MyStack<E>(vararg items: E){

    private val elements = items.toMutableList()

    fun push(element: E) = elements.add(element)

    fun peek(): E = elements.last()

    fun pop(): E = elements.removeAt(elements.size -1)

    fun isEmpty() = elements.isEmpty()

    fun size() = elements.size

    override fun toString() = "MyStack(${elements.joinToString()})"
}

fun <E> myStackOf(vararg elements: E) = MyStack(*elements)

fun main(args: Array<String>) {

    val stack = myStackOf("Hello", "Bonjour", "Bom dia", "Buon giorno")
    val intStack = myStackOf(1,3,1325,63,14,462,13)

    println(stack.toString())
    println(intStack.toString())
}