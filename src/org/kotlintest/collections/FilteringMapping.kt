package org.kotlintest.collections

//------------------- Filter tests -------------------------//
fun List<Int>.print() {
    this.forEach {
        i -> println("$i")
    }
}

val positiveCheck: (Int) -> Boolean = {a -> a > 0}
val negativeCheck: (Int) -> Boolean = {a -> a > 0}

fun filterTests() {
    val numbers = listOf(1,2,41,1,2,4,-3,-5)
    val positives = numbers.filter { x -> x > 0 }
    val negatives = numbers.filter { it < 0 }

    println("Positives :")
    positives.print()

    println("Negatives :")
    negatives.print()

    // Map the numbers to their opposite
    val opposite = numbers.map { it*(-1) }
    println("Opposites :")
    opposite.print()

    // Check if there are any negatives
    println(numbers.any { negativeCheck(it) })
    println(positives.any { negativeCheck(it) })

    // Check if they are all positive
    println(numbers.all { positiveCheck(it) })
    println(positives.all { positiveCheck(it) })

    // Check if there none that are positive
    println(numbers.none { positiveCheck(it) })
    println(negatives.none { positiveCheck(it) })
}

fun main (args: Array<String>) {
    filterTests()
}