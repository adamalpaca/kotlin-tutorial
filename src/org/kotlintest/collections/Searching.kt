package org.kotlintest.collections

data class Person(val name: String, val city: String, val phone: String) // 1

val people = listOf(                                                     // 2
        Person("John", "Boston", "+1-888-123456"),
        Person("Sarah", "Munich", "+49-777-789123"),
        Person("Svyatoslav", "Saint-Petersburg", "+7-999-456789"),
        Person("Vasilisa", "Saint-Petersburg", "+7-999-123456"))

// Using find and findLast in an array of Strings
fun main (args: Array<String>) {

    // Search terms
    val words = listOf("Hello", "Let's", "try", "and", "find", "something", "somehow")
    val first = words.find { it.startsWith("some") }
    val last = words.findLast { it.startsWith("some") }
    val nothing = words.find { it.startsWith("nothing") }
    println("These are our results: $first, $last and $nothing")

    // First and last
    val firstInList = words.first()
    val lastInList = words.last()
    val firstOrNull = words.firstOrNull { it.startsWith('z') }
    println("First in list : $firstInList, last : $lastInList, first starting with 'z' : $firstOrNull")

    // Counting with or without predicates
    val totalCount = words.count()
    val subCount = words.count { it.startsWith('s') }

    println("Total: $totalCount, amount starting with 's': $subCount")

    // Associations
    val phoneBook = people.associateBy { it.phone }
    val cityBook = people.associateBy(Person::phone, Person::city)
    val peopleCities = people.groupBy(Person::city, Person::name)

    println(cityBook.toString())
    println(peopleCities.toString())

    // Partition
    val numbers = listOf(1,2,3,5,5,3,2,5,1,3,523,6,32461,3,62,6,47,26,2,262,526,413,4763)
    val (odd, even) = numbers.partition { it % 2 == 1 }
    println("Odd : ${odd.toString()}")
    println("Even : ${even.toString()}")
}