package org.kotlintest.collections

fun main (args: Array<String>) {

    // Flat maps  --- Creates a singular list and NOT a list of lists
    val numbers = listOf(1,4,7,10)
    val interpolated = numbers.flatMap { listOf(it, it+1, it+2) }
    println(interpolated.toString())

    // Sorting
    val shuffled = listOf(6,21,6,15,7,46,1,7,2)
    val natural = shuffled.sorted()
    val inverted = shuffled.sortedBy { -it }
    println(natural.toString())
    println(inverted.toString())

    // Map element access
    val map = mapOf("Key 1" to "Jan", "Key 2" to "Michael", "Key 3" to "Vincent")
    val name1 = map["Key 1"]
    println(name1)
    val mapWithDefault = map.withDefault { k -> k.length }
    val name2 = mapWithDefault.getValue("Kedvsy 2")

    // Just prints the default because the key can't be found
    println(name2)


    // Zip two array together
    val A = listOf("a", "b", "c", "d")
    val B = listOf(1, 2, 3, 4)

    val resultPairs = A zip B
    println(resultPairs)

    val resultPairsReduce = A.zip(B) {a, b -> "$a, $b"}
    println(resultPairsReduce)


    // Get or else
    val list = listOf (9, 18, 27)
    println(list.getOrElse(1) {a -> (a+1)*9})
    println(list.getOrElse(3) {a -> (a+1)*9})

    val mutMap = mutableMapOf<String, Int?>()
    println(mutMap.getOrElse("x") { 42 })

    mutMap["x"] = 3
    println(mutMap.getOrElse("x") { 42 })

    mutMap["x"] = null
    println(mutMap.getOrElse("x") { 42 })
}