package org.kotlintest.collections

//------------------- List tests ---------------------------//
// A list is an ordered collection
// List is read-only, MutableList is read/write

val systemUsers: MutableList<Int> = mutableListOf(1, 2, 3)
val sudoers: List<Int> = systemUsers

fun addSudoer(newUser: Int) {
    systemUsers.add(newUser)
}

fun getSysSudoers(): List<Int> {
    return sudoers
}

fun listTests() {
    addSudoer(4)
    println("Tot sudoers: ${getSysSudoers().size}")
    getSysSudoers().forEach {
        i -> println("Some useful info on user $i")
    }
}


//------------------- Set tests ----------------------------//
// A set is an unordered collection (doesn't support duplicates)
// Set is read-only, MutableSet is read/write

fun <T> MutableSet<T>.addElement(uniqueElement: T): Boolean {
    return this.add(uniqueElement)
}

fun getStatusLog(isAdded: Boolean): String {
    return if (isAdded) "registered correctly." else "marked as duplicate and rejected"
}

fun setTests() {
    val setTest: MutableSet<String> = mutableSetOf("Alpha", "Beta", "Gamma")

    val aNewString: String = "Hello"
    val aStringAlreadyIn: String = "Beta"

    println("String $aNewString ${getStatusLog(setTest.addElement(aNewString))}")
    println("String $aStringAlreadyIn ${getStatusLog(setTest.addElement(aStringAlreadyIn))}")
}


//------------------- Map tests ----------------------------//
// A map is a collection of key/value pairs.
// Map is read-only, MutableMap is read/write

const val POINTS_X_PASS: Int = 15
val EZPassAccounts: MutableMap<String, Int> = mutableMapOf("John" to 100, "Jebidiah" to 100, "Jesus" to 100)
val EZPassReport: Map<String, Int> = EZPassAccounts

fun updatePointsCredit(accountId: String) {
    if (EZPassAccounts.containsKey(accountId)) {
        println("Updating $accountId...")
        EZPassAccounts[accountId] = EZPassAccounts.getValue(accountId) + POINTS_X_PASS
    } else {
        println("Error: Trying to update a non-existing account (id: $accountId)")
    }
}

fun accountsReport() {
    println("EZ-Pass report:")
    EZPassReport.forEach {
        k, v -> println("ID $k: credit $v")
    }
}

fun mapTests() {
    accountsReport()
    updatePointsCredit("John")
    updatePointsCredit("John")
    updatePointsCredit("Jeffrey")
    accountsReport()
}

fun main (args: Array<String>) {
    listTests()
    setTests()
    mapTests()
}