package org.kotlintest.scopefunctions

enum class FightingStyle {
    BRAWLER, AGILE, SHOWBOATER, DIRTY
}

class Fighter() {
    var name = ""
    var style = FightingStyle.BRAWLER
    var age = 30
    var healthBar = 100
    var ko = false

    override fun toString() = "Name $name, Age: $age, Fighting Style: $style"

    fun receiveHit(hp: Int) {
        healthBar -= hp

        if (healthBar < 0) {
            healthBar = 0
            ko = true
//            println("$name is KO")
        }
/*        else {
            println("$name has $healthBar hp points left")
        }*/
    }
}

fun main( args: Array<String> ) {

    val fighter1 = Fighter()
    val fighter2 = Fighter()

    val stringDescription1 = fighter1.apply {
        name = "John Cena"
        age = 38
    }.toString()

    val stringDescription2 = fighter2.apply {
        name = "Rey Myterio"
        age = 32
        style = FightingStyle.AGILE
    }.toString()

    println(stringDescription1)
    println(stringDescription2)

    fighter2.receiveHit(10)
    fighter2.receiveHit(100)

    with(fighter2) {
        if(ko)
            println("$name is KO")
        else
            println("$name has $healthBar hp points left")
    }
}

