package org.kotlintest.scopefunctions

fun main(args: Array<String>) {
    getNullableLength(null)
    getNullableLength("")
    getNullableLength("any old string")
}

fun getNullableLength(ns: String?) {
    println("for \"ns\":")
    ns?.run {
        println("\tis empty? " + isEmpty())
        println("\tlength = $length")
        length
    }
}