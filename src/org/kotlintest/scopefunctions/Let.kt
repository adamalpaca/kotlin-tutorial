package org.kotlintest.scopefunctions

fun main(args: Array<String>) {

    val empty = "".let {
        customPrint(it)
        it.isEmpty()
    }
    println(" is empty: $empty")

    printNonNull(null)
    printNonNull("hello")
}

fun printNonNull(str: String?) {
    println("Printing \"str\":")

    str?.let {
        print("\t")
        customPrint(it)
        println()
    }
}

fun customPrint(str: String) {
    println(str)
}