package org.kotlintest.specialclasses

// Singleton pattern using the object keyword
object Singleton {
    private var messages : MutableList<String> = mutableListOf()

    fun addMessage(message: String) {
        messages.add(messages.size, message)
    }

    fun getMessages(): MutableList<String> = messages

    fun printMessages() { for (i in messages) println(i) }
}

// Creating a class with a companion object, this is similar to static members in Java
class Node {

    // What is the node id
    val instanceNumber = newInstance()

    // "Static" counter for the amount of nodes
    companion object InstanceCounter {
        var instances = 0

        fun newInstance(): Int {
            instances++
            return instances-1
        }
    }

    fun printThis() {
        println("This is the node number : $instanceNumber of $instances nodes")
    }
}

fun main (args: Array<String>) {

    // Using a singleton pattern
    Singleton.addMessage("Hello")
    Singleton.addMessage("Bonjour")
    Singleton.addMessage("Guten Tag")
    Singleton.printMessages()
    Singleton.addMessage("Snoop doggy")
    Singleton.printMessages()

    // Using an inline object declaration
    val inlineObjectConstants = object {
        var hyperlink: String = "https://duckduckgo.com/"
        var maximumDaysInAMonth: Int = 31
        val anotherNestedObject = object {
            val name: String = "Nested boy"
        }

        fun printThis() {
            println("Link: $hyperlink,\nMax days in a month: $maximumDaysInAMonth,\nNested object's name: ${anotherNestedObject.name}")
        }
    }
    inlineObjectConstants.printThis()

    // Create some nodes
    val node0 = Node()
    val node1 = Node()
    node0.printThis()
    val node2 = Node()
    node1.printThis()
    node2.printThis()
}