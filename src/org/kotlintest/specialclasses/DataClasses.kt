package org.kotlintest.specialclasses

data class User(val name: String, val id: Int)

fun main (args : Array<String>) {
    val user = User("Adam", 2)
    val user2 = User("Seb", 3)
    val user3 = User("Abbie", 1)
    val user4 = User("Adam", 2)

    println("user == user2: ${user == user2}")
    println("user == user4: ${user == user4}")

    println(user.hashCode())
}