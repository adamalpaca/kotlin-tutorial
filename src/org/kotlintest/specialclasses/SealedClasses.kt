package org.kotlintest.specialclasses

sealed class Bird(val name: String)

// All of these subclasses must remain in the same file as the parent
class Owl(val owlName: String, val message: String) : Bird(owlName)
class Penguin(val penguinName: String) : Bird(penguinName)

fun greetBird(bird: Bird): String {
    when(bird) {
        is Owl -> return "Hello ${bird.name}, is this a message for me ? : ${bird.message}"
        is Penguin -> return "Hello ${bird.name}"

        // With a non sealed class, "else" is required
    }
}

fun main (args : Array<String>) {
    println(greetBird(Owl("Errol", "HOW DARE YOU STEAL THAT CAR?!?!")))
}