package org.kotlintest.specialclasses

enum class Days {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}

enum class Color(val rgb: Int) {
    RED(0xFF0000),
    GREEN(0x00FF00),
    BLUE(0x0000FF),
    YELLOW(0xFFFF00);

    fun containsRed() = (this.rgb and 0xFF0000 != 0)
}

fun main (args : Array<String>) {

    // Day enum tests
    val day = Days.MONDAY

    val message = when(day) {
        Days.MONDAY -> "I hate Mondays"
        Days.TUESDAY -> "Mardi gras"
        Days.WEDNESDAY -> "Weekly Hump"
        Days.THURSDAY -> "Jeudredi"
        Days.FRIDAY -> "Weekend!"
        Days.SATURDAY -> "Lazy days"
        Days.SUNDAY -> "I'm so sorry, Jon ..."
    }

    println(message)

    // Colour enum tests
    val red = Color.RED
    println(red)
    println(red.containsRed())
    println(Color.BLUE.containsRed())

}